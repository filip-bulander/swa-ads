# Advertisement API

Application is using Python with Flask framework. It can be run by using Docker. There is Dockerfile available. 
Repo is pushed in DockerHub: https://hub.docker.com/repository/docker/19960701/swa-ads.

## RUN
```
docker build -t my_docker_flask:latest .
docker run -d -p 9698:9698 my_docker_flask:latest
```
Running on `localhost:9698`

## API Definition
### Models
#### Car
`name` - String
`photo` - String
`price` - Integer

### Endpoints
#### Car list 
`localhost:9698/cars` - GET

Returns list of Cars

#### Add car
`localhost:9698/cars` - POST
Body - `Car` - All parameters are required

Returns added Car

#### Get car
`localhost:9698/cars/{id}` - GET

Returns Car identified by id

#### Delete car
`localhost:9698/cars/{id}` - DELETE

Deletes Car identified by id

#### Update car
`localhost:9698/cars/{id}` - PATCH

Body - parameters from Car model - every parameters are optional

Updates Car identified by id
