import os
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DATABASE_URL", app.config.get("DATABASE_URL"))
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)


class Car(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    photo = db.Column(db.String(255))
    price = db.Column(db.Integer)

    def __repr__(self):
        return '<Car %s>' % self.name


class CarSchema(ma.Schema):
    class Meta:
        fields = ("id", "name", "photo", "price")


car_schema = CarSchema()
cars_schema = CarSchema(many=True)


class CarListResource(Resource):
    def get(self):
        cars = Car.query.all()
        return cars_schema.dump(cars)

    def post(self):
        new_car = Car(
            name=request.json['name'],
            photo=request.json['photo'],
            price=request.json['price']
        )
        db.session.add(new_car)
        db.session.commit()
        return car_schema.dump(new_car)


class CarResource(Resource):
    def get(self, car_id):
        car = Car.query.get_or_404(car_id)
        return car_schema.dump(car)

    def patch(self, car_id):
        car = Car.query.get_or_404(car_id)

        if 'name' in request.json:
            car.name = request.json['name']
        if 'photo' in request.json:
            car.photo = request.json['photo']
        if 'price' in request.json:
            car.price = request.json['price']

        db.session.commit()
        return car_schema.dump(car)

    def delete(self, car_id):
        car = Car.query.get_or_404(car_id)
        car.delete()
        db.session.commit()
        return '', 204


api.add_resource(CarListResource, '/cars')
api.add_resource(CarResource, '/cars/<int:car_id>')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=9698)
